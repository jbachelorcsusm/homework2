# Homework 2: Solution #

This is one (of an astronomically large number) of the possible ways to implement a basic calculator. I stuck largely with things we have covered in class thus far, but I expanded on the solution a bit here and there just to introduce you to a few new features of Xamarin.Forms. Read on to see what to look for!

***

![Basic Calculator on Android and iOS](Homework2BothPlatforms.png "App running on Android and iOS")

***

## Neat Stuff to Look For
### App.xaml
* This is a great location to place any app-wide styling. In the current instance, it was brought to my attention that Android has some default margins applied to their native buttons, and Xamarin.Forms does not tap into that. Therefore, even specifying margins of 0 will not remove the space between buttons. Since this made the UI look kind of clumsy, I decided to create an [implicit style](https://developer.xamarin.com/guides/xamarin-forms/user-interface/styles/implicit/) for all buttons, setting the margins to negative values on Android to make up for the margin applied automatically.
    * Note that we could have written a [CustomRenderer](https://developer.xamarin.com/guides/xamarin-forms/application-fundamentals/custom-renderer/) instead, but that is much more work than it would be worth when such a simple workaround is available.

### Homework2Page.xaml
* Here you can see a ResourceDictionary applied at the page level. This allowed me to create styling for the various button types throughout the page without manually entering the same properties and values over and over again.
    * Within this resource dictionary is an example of [style inheritance](https://developer.xamarin.com/guides/xamarin-forms/user-interface/styles/inheritance/). Look for the "BasedOn" keyword to see where one style is quite literally *based on* another. The goal is to keep your code DRY:  Don't Repeat Yourself!

### Homework2Page.xaml.cs
* Example of a very basic state machine.
* DisplayAlert samples... Thrown together to demonstrate how to lose all your users by annoying them to death.
* DisplayActionSheet sample.
