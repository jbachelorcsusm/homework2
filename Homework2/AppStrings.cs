﻿using System;

namespace Homework2
{
    public static class AppStrings
    {
        public static string WelcomeText = "hi there!";
        public static string EverySeventhTimeOrSomethingReallyStrangeHappened
        = "Woohoo! You've probably hit the add button 7 times. It's either that, or something has gone seriously awry.";
        public static string ConfusedTitle = "Fluffy Bunny!";
        public static string ErrorTitle = "Something Bad Happened";
        public static string ErrorMessage = "Sorry... There appears to be a bug in the code!";
        public static string DisclaimerMessage = "Disclaimer:  There are zero unit tests on this code. Zero percent code coverage. So if you haven't hit the add button some multiple of 7 times, it's time to start bug-hunting!";
        public static string ImSuchAJerk = "I'm Such a Jerk";
        public static string Annoyance = "Pop-ups get annoying pretty fast, huh? Remember to use them sparingly!";
        public static string PatienceTesting = "Oh, really?  Well then how about another pop-up?";
        public static string HowAboutNow = "How about now?";
        public static string Ok = "Whatev";
        public static string Nah = "Nah";
        public static string YES = "YES!!";
        public static string EnoughAlready = "Enough Already!";
        public static string DisclaimerTitle = "Disclaiming like a boss";
        public static string TooManyOperandsTitle = "There Can Be Only 2";
        public static string TooManyOperandsMessage = "Sorry... This is a simple calculator. We only deal with two operands at a time.";
        public static string DivideByZeroError = "NaN";
    }
}
