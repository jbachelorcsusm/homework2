﻿using System;

namespace Homework2
{
    public static class Operators
    {
        public const string Addition = "+";
        public const string Subtraction = "-";
        public const string Multiplication = "*";
        public const string Division = "/";
    }
}
