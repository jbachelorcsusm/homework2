﻿using System;
namespace Homework2
{
    public enum CalculatorState
    {
        WelcomeTextOnDisplay,
        EnteringOperand1,
        EnteringOperand2,
        OperatorTapped,
        SolutionComputedOrCalculatorCleared,
    }
}
