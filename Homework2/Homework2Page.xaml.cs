﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Homework2
{
    public partial class Homework2Page : ContentPage
    {
        #region Properties, fields, and constructors

        int _addButtonTappedCounter = 0; // just for some annoyance fun!

        int _operandOne = 0;
        int _operandTwo = 0;
        string _operatorTapped = string.Empty;
        CalculatorState _calculatorState;

        public Homework2Page()
        {
            InitializeComponent();
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Homework2Page)}:  ctor");

            CalculatorDisplayLabel.Text = AppStrings.WelcomeText;
            UpdateCalculatorState(CalculatorState.WelcomeTextOnDisplay);
        }

        #endregion Properties, fields and constructors

        #region Number button handlers

        void On0Tapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(On0Tapped)}");
            AddNumberToDisplay(0);
        }

        void On1Tapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(On1Tapped)}");
            AddNumberToDisplay(1);
        }

        void On2Tapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(On2Tapped)}");
            AddNumberToDisplay(2);
        }

        void On3Tapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(On3Tapped)}");
            AddNumberToDisplay(3);
        }

        void On4Tapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(On4Tapped)}");
            AddNumberToDisplay(4);
        }

        void On5Tapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(On5Tapped)}");
            AddNumberToDisplay(5);
        }

        void On6Tapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(On6Tapped)}");
            AddNumberToDisplay(6);
        }

        void On7Tapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(On7Tapped)}");
            AddNumberToDisplay(7);
        }

        void On8Tapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(On8Tapped)}");
            AddNumberToDisplay(8);
        }

        void On9Tapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(On9Tapped)}");
            AddNumberToDisplay(9);
        }

        #endregion Numerical button handlers

        #region Non-Numerical button handlers

        async Task OnAddTapped(object sender, System.EventArgs e)
        {
            await OnOperatorTapped(Operators.Addition);

            if (++_addButtonTappedCounter % 7 == 0) // purely just for fun... And to further demo alerts & actionSheets.
            {
                await AnnoyUserToThePointOfTears();
            }
        }

        async Task OnSubtractTapped(object sender, System.EventArgs e)
        {
            await OnOperatorTapped(Operators.Subtraction);
        }

        async Task OnMultiplyTapped(object sender, System.EventArgs e)
        {
            await OnOperatorTapped(Operators.Multiplication);
        }

        async Task OnDivideTapped(object sender, System.EventArgs e)
        {
            await OnOperatorTapped(Operators.Division);
        }

        async void OnComputeTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnComputeTapped)}");
            if (_calculatorState == CalculatorState.SolutionComputedOrCalculatorCleared
               || _calculatorState == CalculatorState.OperatorTapped)
            {
                return;
            }

            _operandTwo = await GetIntFromString(CalculatorDisplayLabel.Text);
            ComputeAndDisplaySolution();
        }

        void OnClearTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnClearTapped)}");

            CalculatorDisplayLabel.Text = "0";
            _operandOne = 0;
            _operandTwo = 0;
            _operatorTapped = string.Empty;

            UpdateCalculatorState(CalculatorState.SolutionComputedOrCalculatorCleared);
        }

        #endregion Non-Numerical button handlers

        #region Other Methods

        private void AddNumberToDisplay(int numberToAdd)
        {
            PreventLeadingZeros();

            switch (_calculatorState)
            {
                case CalculatorState.WelcomeTextOnDisplay:
                case CalculatorState.SolutionComputedOrCalculatorCleared:
                    CalculatorDisplayLabel.Text = string.Empty;
                    UpdateCalculatorState(CalculatorState.EnteringOperand1);
                    break;
                case CalculatorState.OperatorTapped:
                    CalculatorDisplayLabel.Text = string.Empty;
                    UpdateCalculatorState(CalculatorState.EnteringOperand2);
                    break;
                default:
                    break;
            }

            CalculatorDisplayLabel.Text += numberToAdd.ToString();
        }

        private void UpdateCalculatorState(CalculatorState newState)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(UpdateCalculatorState)}:  {newState}");
            _calculatorState = newState;
        }

        private void PreventLeadingZeros()
        {
            if (string.Equals(CalculatorDisplayLabel.Text, "0"))
            {
                CalculatorDisplayLabel.Text = string.Empty;
            }
        }

        private async Task OnOperatorTapped(string theOperator)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnOperatorTapped)}:  {theOperator}");
            int operandFromText = await GetIntFromString(CalculatorDisplayLabel.Text);

            if (_calculatorState == CalculatorState.OperatorTapped)
            {
                return;
            }

            switch (_calculatorState)
            {
                case CalculatorState.SolutionComputedOrCalculatorCleared:
                case CalculatorState.EnteringOperand1:
                    _operandOne = operandFromText;
                    _operandTwo = 0;
                    break;
                case CalculatorState.EnteringOperand2:
                    _operandTwo = operandFromText;
                    double answer = ComputeAndDisplaySolution();
                    _operandOne = (int)answer; // This would be no good if this were a real calculator, which would handle decimals with a lot more grace.
                    _operandTwo = 0;
                    break;
                default:
                    break;
            }

            _operatorTapped = theOperator;
            UpdateCalculatorState(CalculatorState.OperatorTapped);
        }

        private double ComputeAndDisplaySolution()
        {
            double solution = 0d;
            bool divisionByZero = false;

            switch (_operatorTapped)
            {
                case Operators.Addition:
                    solution = _operandOne + _operandTwo;
                    break;
                case Operators.Subtraction:
                    solution = _operandOne - _operandTwo;
                    break;
                case Operators.Multiplication:
                    solution = _operandOne * _operandTwo;
                    break;
                case Operators.Division:
                    if (_operandTwo == 0)
                    {
                        divisionByZero = true;
                    }
                    else
                    {
                        solution = _operandOne / _operandTwo;
                    }
                    break;
                default:
                    break;
            }

            if (divisionByZero == false)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(ComputeAndDisplaySolution)}:  {_operandOne} {_operatorTapped} {_operandTwo} = {solution}");

                CalculatorDisplayLabel.Text = solution.ToString();
            }
            else
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(ComputeAndDisplaySolution)}:  Agh!! Division by zero!");

                CalculatorDisplayLabel.Text = AppStrings.DivideByZeroError;
            }

            UpdateCalculatorState(CalculatorState.SolutionComputedOrCalculatorCleared);

            return solution;
        }

        private async Task<int> GetIntFromString(string textToConvert)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetIntFromString)}");

            bool successfullyConvertedToInt = true;
            int intFromString = 0;

            if (textToConvert.Equals(AppStrings.WelcomeText) == false
                && textToConvert.Equals(AppStrings.DivideByZeroError) == false)
            {
                successfullyConvertedToInt =
                    Int32.TryParse(CalculatorDisplayLabel.Text, out intFromString);
            }

            if (successfullyConvertedToInt == false)
            {
                await DisplayAlert(AppStrings.ErrorTitle,
                                  AppStrings.ErrorMessage,
                                   AppStrings.Ok);
            }

            return intFromString;
        }

        private async Task AnnoyUserToThePointOfTears()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(AnnoyUserToThePointOfTears)}");
            await DisplayAlert(
                                AppStrings.ConfusedTitle,
                                AppStrings.EverySeventhTimeOrSomethingReallyStrangeHappened,
                                AppStrings.Ok);

            await DisplayAlert(
                AppStrings.DisclaimerTitle,
                AppStrings.DisclaimerMessage,
                AppStrings.Ok);

            var userIsBoredAndPossiblyAngry = false;

            while (userIsBoredAndPossiblyAngry == false)
            {
                userIsBoredAndPossiblyAngry = await DisplayAlert(
                AppStrings.ImSuchAJerk,
                AppStrings.Annoyance,
                AppStrings.YES,
                AppStrings.Nah);

                if (userIsBoredAndPossiblyAngry)
                {
                    break;
                }

                var userResponse = await DisplayActionSheet(AppStrings.HowAboutNow,
                                                     AppStrings.YES,
                                                     AppStrings.EnoughAlready,
                                                     AppStrings.Nah,
                                                      AppStrings.Ok);
                if (userResponse.Equals(AppStrings.YES)
                   || userResponse.Equals(AppStrings.EnoughAlready))
                {
                    userIsBoredAndPossiblyAngry = true;
                }
            }
        }


        #endregion Other Methods
    }
}
